Nodereference Quantifier README
---------------------------------------

If you have ever faced the problem of adding extra information to a nodereference, e. g. an amount, tag or 
booking information, there hasn't been an easy solution yet. This module aims at filling in this particular 
niche by introducing a new CCK field type inheriting as much as possible from standard nodereference fields.
Major features include:

- CCK field type "Node reference Quantifier"
- Customizable form field appearance
- Various field formatters
- Re-use of standard nodereference widgets and its settings
- Integration widget provided by Nodereference Explorer http://drupal.org/project/nodereference_explorer

On the node edit form editors can enter abitrary information into a simple textfield next to a nodereference
widget. Field formatters let you customize the appearance when viewing the node.

This project has been initial sponsored by David Malan from Codevelopment, http://www.codevelopment.com.au.


Usage
-----

This module just supplies a new CCK field type with an extra subwidget and settings. For questions concerning the
administration, theming and customization of CCK fields I refer to the official CCK project http://drupal.org/project/cck
and handbook http://drupal.org/node/101723.


Authors
-------
Gottfried Nindl (gnindl: http://drupal.org/user/421442)